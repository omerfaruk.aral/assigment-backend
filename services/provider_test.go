package services_test

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/pact-foundation/pact-go/utils"
)

var id = 0


func TestPactProvider(t *testing.T) {

	if os.Getenv("CI") != "" {
		t.Skip("Skipping testing in CI environment")
	}
	pact := createPact()

	go startServer()

	f := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.Header.Add("Authorization", "Bearer 1234-dynamic-value")
			next.ServeHTTP(w, r)
		})
	}

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://localhost:9876",
		PactURLs:                   []string{"https://pass.pactflow.io/pacts/provider/Api/consumer/Client/latest"},
		BrokerToken:                "2icPN_--1IqrEoEJcit3DA",
		PublishVerificationResults: true,
		ProviderVersion:            "2.0.0",
		RequestFilter:              f,
		StateHandlers: types.StateHandlers{
			"hello world todo object": func() error {
				id = 0
				title = "Task 1"
				return nil
			},
		},
	})

	if err != nil {
		t.Log(err)
	} else {
		log.Printf("API terminating:")
	}
}

var title = "Task 1"

func startServer() {
	mux := http.NewServeMux()

	mux.HandleFunc("/getTask", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Add("Content-Type", "application/json; charset=utf-8")
		w.Header().Add("Access-Control-Allow-Origin", "*")
		fmt.Fprintf(w, fmt.Sprintf(`[{"id": %d, "title": "%s"}]`, id, title))
	})

	log.Fatal(http.ListenAndServe("localhost:9876", mux))
}

var dir, _ = os.Getwd()
var logDir = fmt.Sprintf("%s/log", dir)
var port, _ = utils.GetFreePort()

// Setup the Pact client.
func createPact() dsl.Pact {
	return dsl.Pact{
		Provider: "Client",
		Consumer: "Api",
		LogDir:   logDir,
		LogLevel: "INFO",
	}
}
