package services

import (
	"encoding/json"
	"main/models"
	"main/repository"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Service struct {
	Repo repository.Repository
}

func (service *Service) PostTasks(c echo.Context) error {
	jsonMap := make(map[string]interface{})
	var index int = 0
	err := json.NewDecoder(c.Request().Body).Decode(&jsonMap)
	if err != nil {
		return err
	}

	id, ok := jsonMap["Id"].(string)
	if !ok {
		println("Error from Id")
	}
	title, ok := jsonMap["Title"].(string)
	if !ok {
		println("Error from Title")
	}

	println(id, title)
	service.Repo.AddToList(models.Task{Id: index, Title: title})
	index++
	return c.JSON(http.StatusCreated, service.Repo.ReturnList())

}

func (service *Service) GetTask(c echo.Context) error {
	return c.JSON(http.StatusOK, service.Repo.ReturnList())
}
