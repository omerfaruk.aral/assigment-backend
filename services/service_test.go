package services_test

import (
	"main/models"
	"main/repository"
	"main/services"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

var (
	mockDB = []models.Task{
		{Id: 0, Title: "Task 1"},
	}
)

func TestPostTasks(t *testing.T) {
	userJSON := `{"Id":0,"Title":"Task 1"}`

	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/postTasks", strings.NewReader(userJSON))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := services.Service{}

	userJSON = "[" + userJSON + "]" + "\n"
	// Assertions
	if assert.NoError(t, h.PostTasks(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.Equal(t, userJSON, rec.Body.String())
	}
}

func TestGetTask(t *testing.T) {
	userJSON := `{"Id":0,"Title":"Task 1"}`

	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/getTask", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	h := services.Service{repository.Repository{mockDB}}
	userJSON = "[" + userJSON + "]" + "\n"
	// Assertions
	if assert.NoError(t, h.GetTask(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, userJSON, rec.Body.String())
	}
}
