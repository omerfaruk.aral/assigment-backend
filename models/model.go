package models

type Task struct {
	Id    int    `json:"Id"`
	Title string `json:"Title"`
}
