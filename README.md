<h1 align="center">Welcome to assigment-backend 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/go-%3E%3D1.17-blue.svg" />
  <a href="https://github.com/kefranabg/readme-md-generator#readme" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://github.com/kefranabg/readme-md-generator/graphs/commit-activity" target="_blank">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" />
  </a>

</p>

> CLI that generates beautiful README.md files.

### 🏠 [Homepage](https://github.com/kefranabg/readme-md-generator#readme)

## Prerequisites

  - go >= 1.17

## To-do App Assigment
 - Backend side was coded with Echo framework.

## Test
 - Backend side was developed using A-TDD approach.
### Unit Test
 - I used httptest and testing frameworks for unit testing
### Provider Test
 - I used pact tool for provider test.

## Install

```sh
go mod tidy
```

## Usage

```sh
go run main.go
```

## Run services test

```sh
go test services/service_test.go
```

## Run repository test

```sh
go test repository/repository_test.go
```

## Run provider test

```sh
go test services/provider_test.go
```
## Author

👤 **ömer faruk aral**

* Gitlab: [@omerfaruk.aral](https://gitlab.com/omerfaruk.aral)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/kefranabg/readme-md-generator/issues). You can also take a look at the [contributing guide](https://github.com/kefranabg/readme-md-generator/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2021 [ömer faruk aral](https://gitlab.com/omerfaruk.aral).<br />
This project is [MIT](https://github.com/kefranabg/readme-md-generator/blob/master/LICENSE) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
