package repository_test

import (
	"main/models"
	"main/repository"
	"testing"
)

func TestAddToList(t *testing.T) {
	var repo repository.Repository
	mockDB := []models.Task{
		{Id: 0, Title: "Task 1"},
	}
	want := models.Task{Id: 0, Title: "Task 1"}

	got := repo.AddToList(want)

	if got[0] != mockDB[0] {
		t.Errorf("got %q, wanted %q", got, want)
	}

}

func TestReturnList(t *testing.T) {
	var repo1 repository.Repository
	wantDB := []models.Task{
		{Id: 0, Title: "Task 1"},
		{Id: 1, Title: "Task 2"},
		{Id: 2, Title: "Task 3"},
		{Id: 3, Title: "Task 4"},
	}

	_ = repo1.AddToList(models.Task{Id: 0, Title: "Task 1"})
	_ = repo1.AddToList(models.Task{Id: 1, Title: "Task 2"})
	_ = repo1.AddToList(models.Task{Id: 2, Title: "Task 3"})
	_ = repo1.AddToList(models.Task{Id: 3, Title: "Task 4"})
	got := repo1.ReturnList()
	if !compareArrays(got, wantDB) {
		t.Errorf("got %q, wanted %q", got, wantDB)
	}

}

func compareArrays(array1, array2 []models.Task) bool {
	var flag bool = true
	for i, _ := range array1 {
		if array1[i] != array2[i] {
			flag = false
		}
	}
	return flag
}
