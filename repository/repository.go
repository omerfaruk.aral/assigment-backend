package repository

import "main/models"

type Repository struct {
	DB []models.Task
}

func (repo *Repository) AddToList(Obj models.Task) []models.Task {
	repo.DB = append(repo.DB, Obj)
	return repo.DB
}

func (repo *Repository) ReturnList() []models.Task {
	return repo.DB
}
